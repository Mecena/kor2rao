﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace KOR2RAO.WebAPI.Models
{
    public class DL
    {
        public static List<Models.ResultSet> GetRoomGuest(int objekt, string soba)
        {
            SqlConnection connection = new SqlConnection();

            connection.ConnectionString = ConfigurationManager.ConnectionStrings["KORRAZVOJ"].ConnectionString;

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "sp_Rec_Kor2RAO_GetRoomGuests";
            command.CommandType = System.Data.CommandType.StoredProcedure;
            //DateTime result = new DateTime();
            //DateTime.TryParse(date, out result);
            command.Parameters.AddWithValue("@Objekt", objekt);
            command.Parameters.AddWithValue("@BrojSobe", soba);
            command.Connection.Open();
            SqlDataReader results = command.ExecuteReader();

            List<Models.ResultSet> listOfResults = new List<Models.ResultSet>();

            while (results.Read())
            {
                listOfResults.Add(new Models.ResultSet()
                {
                    BrojSobe = string.Concat(results["BrojSobe"]),
                    DatumDol = (DateTime)results["DatumDol"],
                    DatumOdl = (DateTime)results["DatumOdl"],
                    FlPromjena = (bool)(results["FlPromjena"] == DBNull.Value ? false : results["FlPromjena"]),
                    GodinaRodjenja = (int?)(results["GodinaRodjenja"] == DBNull.Value ? (object)null : results["GodinaRodjenja"]),
                    Ime = string.Concat(results["Ime"]),
                    Prezime = string.Concat(results["Prezime"]),
                    KorStatusGosta = string.Concat(results["KorStatusGosta"]),
                    KorTipGosta = string.Concat(results["KorStatusGosta"]),
                    RecGostID = (int)results["RecGostID"],
                    RequestID = (Guid)results["RequestID"]

                });

            }

            return listOfResults;
        }
        public static List<Models.ResultSet> GetGuestsChanges(int objekt, DateTime datum, string soba)
        {
            SqlConnection connection = new SqlConnection();

            connection.ConnectionString = ConfigurationManager.ConnectionStrings["KORRAZVOJ"].ConnectionString;

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "sp_Rec_Kor2RAO_GetGuestsChanges";
            command.CommandType = System.Data.CommandType.StoredProcedure;
            //DateTime result = new DateTime();
            //DateTime.TryParse(date, out result);
            command.Parameters.AddWithValue("@Objekt", objekt);
            command.Parameters.AddWithValue("@Datum", datum);
            command.Parameters.AddWithValue("@BrojSobe", string.IsNullOrEmpty(soba) ? (object)null : soba);
            command.Connection.Open();
            SqlDataReader results = command.ExecuteReader();

            List<Models.ResultSet> listOfResults = new List<Models.ResultSet>();

            while (results.Read())
            {
                listOfResults.Add(new Models.ResultSet()
                {
                    BrojSobe = string.Concat(results["BrojSobe"]),
                    DatumDol = (DateTime)results["DatumDol"],
                    DatumOdl = (DateTime)results["DatumOdl"],
                    FlPromjena = (bool)(results["FlPromjena"] == DBNull.Value ? false : results["FlPromjena"]),
                    GodinaRodjenja = (int?)(results["GodinaRodjenja"] == DBNull.Value ? (object)null : results["GodinaRodjenja"]),
                    Ime = string.Concat(results["Ime"]),
                    Prezime = string.Concat(results["Prezime"]),
                    KorStatusGosta = string.Concat(results["KorStatusGosta"]),
                    KorTipGosta = string.Concat(results["KorStatusGosta"]),
                    RecGostID = (int)results["RecGostID"],
                    RequestID = (Guid)results["RequestID"]

                });

            }

            return listOfResults;
        }
    }
}