﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KOR2RAO.WebAPI.Models
{
    public class ResultSet
    {
        public Guid RequestID { get; set; }
        public string BrojSobe { get; set; }
        public int RecGostID { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public int? GodinaRodjenja { get; set; }
        public DateTime DatumDol { get; set; }
        public DateTime DatumOdl { get; set; }
        public string KorStatusGosta { get; set; }
        public string KorTipGosta { get; set; }
        public bool FlPromjena { get; set; }
    }
}