﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KOR2RAO.WebAPI.Controllers
{
    public class MainController : ApiController
    {
        [Route("kor2rao/getroomguests")]
        [HttpGet]
        public IHttpActionResult GetRoomGuests([FromUri]int objekt, [FromUri]string soba)
        {
            List<Models.ResultSet> res = Models.DL.GetRoomGuest(objekt, soba);

            return Ok(res);
        }

        [Route("kor2rao/getguestschanges")]
        [HttpGet]
        public IHttpActionResult GetGuestsChanges([FromUri]int objekt, [FromUri]DateTime datum, [FromUri]string soba="")
        {
            List<Models.ResultSet> res = Models.DL.GetGuestsChanges(objekt, datum, soba);

            return Ok(res);
        }

        private static string GetRoomGuestJSON(int objekt, string soba)
        {
            string json = JsonConvert.SerializeObject(Models.DL.GetRoomGuest(objekt, soba));
            return json;
        }        
    }
}
