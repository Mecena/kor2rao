﻿using System.Collections.Generic;
using System.Web.Http;

namespace KOR2RAO.WebAPI.Controllers
{
    public class TestController : ApiController
    {
        [HttpGet]
        [Route("kor2rao/test")]
        public IHttpActionResult Get()
        {

            return Ok(new List<int>() { 1, 2, 3 });
        }
    }
}
